'use strict';

customElements.define('compodoc-menu', class extends HTMLElement {
    constructor() {
        super();
        this.isNormalMode = this.getAttribute('mode') === 'normal';
    }

    connectedCallback() {
        this.render(this.isNormalMode);
    }

    render(isNormalMode) {
        let tp = lithtml.html(`
        <nav>
            <ul class="list">
                <li class="title">
                    <a href="index.html" data-type="index-link">proyecto-de-angular documentation</a>
                </li>

                <li class="divider"></li>
                ${ isNormalMode ? `<div id="book-search-input" role="search"><input type="text" placeholder="Type to search"></div>` : '' }
                <li class="chapter">
                    <a data-type="chapter-link" href="index.html"><span class="icon ion-ios-home"></span>Getting started</a>
                    <ul class="links">
                        <li class="link">
                            <a href="overview.html" data-type="chapter-link">
                                <span class="icon ion-ios-keypad"></span>Overview
                            </a>
                        </li>
                        <li class="link">
                            <a href="index.html" data-type="chapter-link">
                                <span class="icon ion-ios-paper"></span>README
                            </a>
                        </li>
                                <li class="link">
                                    <a href="dependencies.html" data-type="chapter-link">
                                        <span class="icon ion-ios-list"></span>Dependencies
                                    </a>
                                </li>
                                <li class="link">
                                    <a href="properties.html" data-type="chapter-link">
                                        <span class="icon ion-ios-apps"></span>Properties
                                    </a>
                                </li>
                    </ul>
                </li>
                    <li class="chapter modules">
                        <a data-type="chapter-link" href="modules.html">
                            <div class="menu-toggler linked" data-bs-toggle="collapse" ${ isNormalMode ?
                                'data-bs-target="#modules-links"' : 'data-bs-target="#xs-modules-links"' }>
                                <span class="icon ion-ios-archive"></span>
                                <span class="link-name">Modules</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                        </a>
                        <ul class="links collapse " ${ isNormalMode ? 'id="modules-links"' : 'id="xs-modules-links"' }>
                            <li class="link">
                                <a href="modules/AppModule.html" data-type="entity-link" >AppModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-AppModule-21493745866e49bbab5c1bd7223ffe4549a97ebd8a0ea2c92c2cb05c15d33b4191b040b154fa4d6916609459951bf9cd14271ec558a2f4bb87d819cd12223dd0"' : 'data-bs-target="#xs-components-links-module-AppModule-21493745866e49bbab5c1bd7223ffe4549a97ebd8a0ea2c92c2cb05c15d33b4191b040b154fa4d6916609459951bf9cd14271ec558a2f4bb87d819cd12223dd0"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-AppModule-21493745866e49bbab5c1bd7223ffe4549a97ebd8a0ea2c92c2cb05c15d33b4191b040b154fa4d6916609459951bf9cd14271ec558a2f4bb87d819cd12223dd0"' :
                                            'id="xs-components-links-module-AppModule-21493745866e49bbab5c1bd7223ffe4549a97ebd8a0ea2c92c2cb05c15d33b4191b040b154fa4d6916609459951bf9cd14271ec558a2f4bb87d819cd12223dd0"' }>
                                            <li class="link">
                                                <a href="components/AjudaComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AjudaComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/AppComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AppComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ClassificacioComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ClassificacioComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/IniciComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >IniciComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/JocComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >JocComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/RegisterComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >RegisterComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/AppRoutingModule.html" data-type="entity-link" >AppRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/GrupmodModule.html" data-type="entity-link" >GrupmodModule</a>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                        'data-bs-target="#directives-links-module-GrupmodModule-7a2d5a77f533b9829c12032ce005dac672134577e118a43a32069dfbf5751fc0178f3abe318b349478187d28fe8be51997bb297cdae04904a9a77f3b0606dbd9"' : 'data-bs-target="#xs-directives-links-module-GrupmodModule-7a2d5a77f533b9829c12032ce005dac672134577e118a43a32069dfbf5751fc0178f3abe318b349478187d28fe8be51997bb297cdae04904a9a77f3b0606dbd9"' }>
                                        <span class="icon ion-md-code-working"></span>
                                        <span>Directives</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="directives-links-module-GrupmodModule-7a2d5a77f533b9829c12032ce005dac672134577e118a43a32069dfbf5751fc0178f3abe318b349478187d28fe8be51997bb297cdae04904a9a77f3b0606dbd9"' :
                                        'id="xs-directives-links-module-GrupmodModule-7a2d5a77f533b9829c12032ce005dac672134577e118a43a32069dfbf5751fc0178f3abe318b349478187d28fe8be51997bb297cdae04904a9a77f3b0606dbd9"' }>
                                        <li class="link">
                                            <a href="directives/ValidarPasswordDirective.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ValidarPasswordDirective</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                        'data-bs-target="#injectables-links-module-GrupmodModule-7a2d5a77f533b9829c12032ce005dac672134577e118a43a32069dfbf5751fc0178f3abe318b349478187d28fe8be51997bb297cdae04904a9a77f3b0606dbd9"' : 'data-bs-target="#xs-injectables-links-module-GrupmodModule-7a2d5a77f533b9829c12032ce005dac672134577e118a43a32069dfbf5751fc0178f3abe318b349478187d28fe8be51997bb297cdae04904a9a77f3b0606dbd9"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-GrupmodModule-7a2d5a77f533b9829c12032ce005dac672134577e118a43a32069dfbf5751fc0178f3abe318b349478187d28fe8be51997bb297cdae04904a9a77f3b0606dbd9"' :
                                        'id="xs-injectables-links-module-GrupmodModule-7a2d5a77f533b9829c12032ce005dac672134577e118a43a32069dfbf5751fc0178f3abe318b349478187d28fe8be51997bb297cdae04904a9a77f3b0606dbd9"' }>
                                        <li class="link">
                                            <a href="injectables/ConnectDBService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ConnectDBService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                </ul>
                </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ? 'data-bs-target="#components-links"' :
                            'data-bs-target="#xs-components-links"' }>
                            <span class="icon ion-md-cog"></span>
                            <span>Components</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="components-links"' : 'id="xs-components-links"' }>
                            <li class="link">
                                <a href="components/LoginComponent.html" data-type="entity-link" >LoginComponent</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ? 'data-bs-target="#classes-links"' :
                            'data-bs-target="#xs-classes-links"' }>
                            <span class="icon ion-ios-paper"></span>
                            <span>Classes</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="classes-links"' : 'id="xs-classes-links"' }>
                            <li class="link">
                                <a href="classes/Usuarisclass.html" data-type="entity-link" >Usuarisclass</a>
                            </li>
                        </ul>
                    </li>
                        <li class="chapter">
                            <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ? 'data-bs-target="#injectables-links"' :
                                'data-bs-target="#xs-injectables-links"' }>
                                <span class="icon ion-md-arrow-round-down"></span>
                                <span>Injectables</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                            <ul class="links collapse " ${ isNormalMode ? 'id="injectables-links"' : 'id="xs-injectables-links"' }>
                                <li class="link">
                                    <a href="injectables/ConnectDBService.html" data-type="entity-link" >ConnectDBService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/LoginServiceService.html" data-type="entity-link" >LoginServiceService</a>
                                </li>
                            </ul>
                        </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ? 'data-bs-target="#miscellaneous-links"'
                            : 'data-bs-target="#xs-miscellaneous-links"' }>
                            <span class="icon ion-ios-cube"></span>
                            <span>Miscellaneous</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="miscellaneous-links"' : 'id="xs-miscellaneous-links"' }>
                            <li class="link">
                                <a href="miscellaneous/functions.html" data-type="entity-link">Functions</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <a data-type="chapter-link" href="coverage.html"><span class="icon ion-ios-stats"></span>Documentation coverage</a>
                    </li>
                    <li class="divider"></li>
                    <li class="copyright">
                        Documentation generated using <a href="https://compodoc.app/" target="_blank" rel="noopener noreferrer">
                            <img data-src="images/compodoc-vectorise.png" class="img-responsive" data-type="compodoc-logo">
                        </a>
                    </li>
            </ul>
        </nav>
        `);
        this.innerHTML = tp.strings;
    }
});