import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterComponent } from './register.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {HttpClientTestingModule} from "@angular/common/http/testing";
import {of} from "rxjs";
import {JocComponent} from "../joc/joc.component";
import {ConnectDBService} from "../../shared/services/connect-db.service";
import {HttpClient} from "@angular/common/http";

describe('RegisterComponent', () => {
  let component: RegisterComponent;
  let fixture: ComponentFixture<RegisterComponent>;
  let service: ConnectDBService;
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [RegisterComponent],
      imports: [ReactiveFormsModule, HttpClientTestingModule, FormsModule],
      providers: [ConnectDBService,HttpClient]
    })
    .compileComponents();

    fixture = TestBed.createComponent(RegisterComponent);
    component = fixture.componentInstance;
    service = TestBed.inject(ConnectDBService);
    fixture.detectChanges();
  });
  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('PARAMETRES REGISTER', () => {
    const res = {data: [{nom:'hector@ies-sabadell.cat',password:'Contraseña123'}]}
    spyOn(service, 'setUser').and.returnValue(of(res.data));
    component.loginForm.patchValue({nom:'hector@ies-sabadell.cat',password:'Contraseña123'});

    component.register();
    fixture.detectChanges();
    expect(component.message).toEqual("Registrado")
  });
});
