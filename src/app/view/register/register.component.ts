import { Component } from '@angular/core';
import { ConnectDBService } from "../../shared/services/connect-db.service";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { Usuarisclass } from "../../shared/clases/usuarisclass";
import { emailValidator, passwordValidator } from "../login/validar-password.directive";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent {
  /**
   * Formulario de registro de usuarios.
   * @type FormGroup
   */
  loginForm!: FormGroup;

  /**
   * Arreglo de usuarios.
   * @type Usuarisclass[]
   */
  users: Usuarisclass[] = [];

  /**
   * Mensaje para mostrar el estado del registro.
   * @type string
   */
  message!: string;

  /**
   * Constructor del componente RegisterComponent.
   * @param connectbd Servicio para conectar con la base de datos.
   */
  constructor(private connectbd: ConnectDBService) { }

  /**
   * Método que se ejecuta al inicializar el componente.
   * Crea el formulario de registro.
   * @returns No devuelve nada.
   */
  ngOnInit(): void {
    this.loginForm = new FormGroup({
      nom: new FormControl('',
        [Validators.minLength(7),
          Validators.required, Validators.email, emailValidator()]),
      password: new FormControl('',
        [Validators.required,
          Validators.minLength(8), passwordValidator()])
    });
  }

  /**
   * Método para registrar un nuevo usuario.
   * @returns No devuelve nada.
   */
  register(): void {
    this.connectbd.setUser(this.loginForm)
      .subscribe(res => {
        this.message = "Registrado";
      });
  }
}
