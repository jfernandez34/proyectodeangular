import { Component, HostListener } from '@angular/core';
import { LoginServiceService } from "../../shared/services/login-service.service";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { emailValidator, passwordValidator } from "../login/validar-password.directive";
import { ConnectDBService } from "../../shared/services/connect-db.service";

@Component({
  selector: 'app-joc',
  templateUrl: './joc.component.html',
  styleUrls: ['./joc.component.css']
})
@HostListener('window:keydown', ['$event'])
export class JocComponent {
  /**
   * Contiene los datos de los usuarios obtenidos de la DB.
   * @type any[]
   */
  public data!: any[];

  /**
   * Mensaje que se muestra al usuario al terminar el juego.
   * @type string
   */
  public message!: string;

  /**
   * Constructor de la clase JocComponent.
   * @param loginservice Servicio de login para comprobar el login del usuario.
   * @param connectdbservice Servicio para conectarse a la base de datos.
   */
  constructor(public loginservice: LoginServiceService, private connectdbservice: ConnectDBService) {}

  /**
   * Matriz que representa el grid del juego.
   * @type string[][]
   * @default tablero vacio
   */
  grid: string[][] = [
    ['#', '#', '#', '#', '#', '#', '#', '#', '#', '#'],
    ['#', '.', '.', '.', '.', '.', '.', '.', '.', '#'],
    ['#', '.', '.', '.', '.', '.', '.', '.', '.', '#'],
    ['#', '.', '.', '.', '.', '.', '.', '.', '.', '#'],
    ['#', '.', '.', '.', '.', '.', '.', '.', '.', '#'],
    ['#', '.', '.', '.', '.', '.', '.', '.', '.', '#'],
    ['#', '.', '.', '.', '.', '.', '.', '.', '.', '#'],
    ['#', '.', '.', '.', '.', '.', '.', '.', '.', '#'],
    ['#', '.', '.', '.', '.', '.', '.', '.', '.', '#'],
    ['#', '@', '.', '.', '.', '.', '.', '.', '.', '#'],
    ['#', '#', '#', '#', '#', '#', '#', '#', '#', '#']
  ];

  /**
   * Formulario de puntos para actualizar en la base de datos.
   * @type FormGroup
   */
  puntsform!: FormGroup;

  /**
   * Indica si el juego está en progreso.
   * @type boolean
   * @default false
   */
  game: boolean = false;

  /**
   * Contador de elementos perdidos.
   * @type number
   * @default 0
   */
  perdidos: number = 0;

  /**
   * Intervalo de tiempo en milisegundos para la caída de elementos.
   * @type number
   * @default 1000
   */
  fallTime: number = 1000;

  /**
   * Intervalo de tiempo en milisegundos para la aparición de nuevos elementos.
   * @type number
   * @default 3000
   */
  spawnTime: number = 3000;

  /**
   * Intervalo que controla la generación de nuevos elementos.
   * @type NodeJS.Timeout
   */
  spawnInterval!: NodeJS.Timeout;

  resposta !: any;
  /**
   * Intervalo que controla la caída de los elementos.
   * @type NodeJS.Timeout
   */
  fallInterval!: NodeJS.Timeout;

  /**
   * Posición del jugador en la matriz grid.
   * @type number[]
   * @default [0, 0]
   */
  posplayer = [0, 0];

  /**
   * Contador de elementos conseguidos por el jugador.
   * @type number
   * @default 0
   */
  conseguidos: number = 0;

  /**
   * Función que inicia el juego, resetea las variables y
   * configura los intervalos para generar y mover elementos.
   * {@link https://developer.mozilla.org/es/docs/Web/API/setInterval}
   * @return No devuelve nada
   */

  jugar() {
    clearInterval(this.spawnInterval);
    clearInterval(this.fallInterval);
    this.message = '';
    this.game = true;
    this.perdidos = 0;
    this.conseguidos = 0;
    this.spawnInterval = setInterval(() => this.spawnNewElement(), this.spawnTime);
    this.fallInterval = setInterval(() => this.fallActity(), this.fallTime);
    this.grid = [
      ['#', '#', '#', '#', '#', '#', '#', '#', '#', '#'],
      ['#', '.', '.', '.', '.', '.', '.', '.', '.', '#'],
      ['#', '.', '.', '.', '.', '.', '.', '.', '.', '#'],
      ['#', '.', '.', '.', '.', '.', '.', '.', '.', '#'],
      ['#', '.', '.', '.', '.', '.', '.', '.', '.', '#'],
      ['#', '.', '.', '.', '.', '.', '.', '.', '.', '#'],
      ['#', '.', '.', '.', '.', '.', '.', '.', '.', '#'],
      ['#', '.', '.', '.', '.', '.', '.', '.', '.', '#'],
      ['#', '.', '.', '.', '.', '.', '.', '.', '.', '#'],
      ['#', '@', '.', '.', '.', '.', '.', '.', '.', '#'],
      ['#', '#', '#', '#', '#', '#', '#', '#', '#', '#']
    ];
  }

  /**
   * Maneja eventos de teclado para mover al jugador dentro del grid.
   * @param event El evento de teclado
   * @return No devuelve nada
   */
  handleKeyDown(event: KeyboardEvent) {
    console.log(this.loginservice.userlogin);
    if (this.game) {
      for (let i = 0; i < this.grid.length; i++) {
        for (let k = 0; k < this.grid[i].length; k++) {
          if (this.grid[i][k] === '@') {
            this.posplayer = [i, k];
          }
        }
      }

      switch (event.code) {
        case 'ArrowLeft':
          if (this.grid[this.posplayer[0]][this.posplayer[1] - 1] !== '#') {
            if (this.grid[this.posplayer[0]][this.posplayer[1] - 1] == 'D') this.conseguidos++;
            this.grid[this.posplayer[0]][this.posplayer[1]] = '.';
            this.grid[this.posplayer[0]][this.posplayer[1] - 1] = '@';
          }
          break;
        case 'ArrowRight':
          if (this.grid[this.posplayer[0]][this.posplayer[1] + 1] !== '#') {
            if (this.grid[this.posplayer[0]][this.posplayer[1] + 1] == 'D') this.conseguidos++;
            this.grid[this.posplayer[0]][this.posplayer[1]] = '.';
            this.grid[this.posplayer[0]][this.posplayer[1] + 1] = '@';
          }
          break;
        default:
          break;
      }
    }
  }

  ngOnInit(){
    this.puntsform = new FormGroup({
      nom: new FormControl(0),
      punts: new FormControl(0)
    });
  }

  /**
   * Función spawn ejecutada en intervalo cada cierto tiempo
   * @return No devuelve nada
   */
  spawnNewElement() {
    this.grid[1][Math.floor(Math.random() * 8) + 1] = "D";
  }

  /**
   * Función fallActity ejecutada en intervalo para mover elementos hacia abajo
   * @return No devuelve nada
   */
  fallActity() {
    for (let i = 0; i < this.grid.length; i++) {
      for (let k = 0; k < this.grid[i].length; k++) {
        if (this.grid[i][k] == 'D') {
          if (this.grid[i + 1][k] == "#") {
            this.perdidos++;
            this.grid[i][k] = '.';
            if (this.perdidos >= 10) {
              this.getUsers();
              clearInterval(this.fallInterval);
              clearInterval(this.spawnInterval);
              this.game = false;
            }
          } else if (this.grid[i + 1][k] == "@") {
            this.conseguidos++;
            if (this.conseguidos % 20 == 0 && this.conseguidos != 0) {
              clearInterval(this.fallInterval);
              clearInterval(this.spawnInterval);
              this.fallTime = this.fallTime / 2;
              this.spawnTime = this.spawnTime / 2;
              this.fallInterval = setInterval(() => this.fallActity(), this.fallTime);
              this.spawnInterval = setInterval(() => this.spawnNewElement(), this.spawnTime);
            }
            this.grid[i][k] = '.';
          } else {
            this.grid[i][k] = ".";
            this.grid[i + 1][k] = "D";
            i++;
          }
        }
      }
    }
  }

  /**
   * Obtiene los usuarios de la DB y comprueba si superan los puntos máximos.
   * @return No devuelve nada
   */
  getUsers() {
    console.log("entra");
    this.connectdbservice.getAllUsers().subscribe(res => {
      if (res.length == 0) {
        this.data = [];
        console.log("vacio");
      } else {
        this.data = res;
        console.log("else");
        console.log(this.puntsform);
        for (let i = 0; i < this.data.length; i++) {
          if (this.data[i].nom == this.loginservice.userlogin.value && this.data[i].punts < this.conseguidos) {
            this.puntsform.controls['punts'].setValue(this.conseguidos);
            this.puntsform.controls['nom'].setValue(this.loginservice.userlogin.value);
            this.updatePuntos();
          }
        }
      }
    });
  }

  /**
   * Actualiza los puntos de la base de datos
   * @return No devuelve nada
   */
  updatePuntos(){
    this.connectdbservice.updatePunts(this.puntsform)
      .subscribe(res => {
        this.resposta = res;
        this.message = "HAS TERMINAT AMB " + this.resposta[0].punts+ " punts";
      });
  }
}
