import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JocComponent } from './joc.component';
import {HttpClientTestingModule} from "@angular/common/http/testing";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {ClassificacioComponent} from "../classificacio/classificacio.component";
import {ConnectDBService} from "../../shared/services/connect-db.service";
import {HttpClient} from "@angular/common/http";
import {of} from "rxjs";

describe('JocComponent', () => {
  let component: JocComponent;
  let fixture: ComponentFixture<JocComponent>;
  let service: ConnectDBService;
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [JocComponent],
      imports: [ReactiveFormsModule, HttpClientTestingModule, FormsModule],
      providers: [ConnectDBService,HttpClient]
    })
      .compileComponents();

    fixture = TestBed.createComponent(JocComponent);
    component = fixture.componentInstance;
    service = TestBed.inject(ConnectDBService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('PARAMETRES ACTUALITZAR', () => {
    const res = {data: [{nom:'hector@ies-sabadell.cat',punts:1000}]}
    spyOn(service, 'updatePunts').and.returnValue(of(res.data));
    component.puntsform.patchValue({nom:'hector@ies-sabadell.cat',punts:1000});

    component.updatePuntos();
    fixture.detectChanges();
    expect(component.message).toEqual("HAS TERMINAT AMB 1000 punts")
  });
});
