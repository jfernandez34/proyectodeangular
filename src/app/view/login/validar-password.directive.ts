import { Directive } from '@angular/core';
import {AbstractControl, ValidationErrors, ValidatorFn} from "@angular/forms";

@Directive({
  selector: '[appValidarPassword]'
})
export class ValidarPasswordDirective {

  constructor() { }
}
export function emailValidator():ValidatorFn {
  return (control: AbstractControl): ValidationErrors | null => {
    const value = control.value;
    if(!value){
      return null
    }
    let domain=[];
    domain = value.split('@');
    console.log(domain[1]);
    const emailvalid = domain[1] == 'ies-sabadell.cat';
    console.log(emailvalid)
    return !emailvalid ? {emailStrength:true}: null;
  }
}
export function passwordValidator():ValidatorFn{
  return (control: AbstractControl):ValidationErrors | null => {
    const value=control.value;
    if(!value){
      return null
    }
    let prohibits=false;
    let especials=[]
    especials=value.split("");
    for(let i=0; i<especials.length; i++){
      if(especials[i]==='@' || especials[i]==='#'){
        prohibits=true;
      }
    }
    const hasUpperCase = /[A-Z]+/.test(value);
    const hasLowerCase = /[a-z]+/.test(value);
    const hasNumeric = /[0-9]+/.test(value);
    const passwordValid = hasUpperCase && hasLowerCase && hasNumeric && !prohibits;

    return !passwordValid ? {passwordStrength:true}: null;
  }
}
