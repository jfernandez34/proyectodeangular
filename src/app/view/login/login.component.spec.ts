import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginComponent } from './login.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {HttpClientTestingModule} from "@angular/common/http/testing";
import {of} from "rxjs";
import {ConnectDBService} from "../../shared/services/connect-db.service";
import {HttpClient} from "@angular/common/http";

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let service: ConnectDBService;
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [],
      imports: [LoginComponent, ReactiveFormsModule, HttpClientTestingModule, FormsModule],
      providers: [ConnectDBService,HttpClient]
    })
    .compileComponents();

    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    service = TestBed.inject(ConnectDBService);
    fixture.detectChanges();
  })

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('PARAMETRES LOGIN', () => {
    const res = {data: [{nom:'hector@ies-sabadell.cat',password:'Caillou_123'}]}
    spyOn(service, 'Login').and.returnValue(of(res.data));
    component.loginForm.patchValue({nom:'hector@ies-sabadell.cat',password:'Caillou_123'});
    component.login();
    fixture.detectChanges();
    expect(component.message).toEqual("L'usuari hector@ies-sabadell.cat s'ha logejat de manera correcte!!!")
  });
});
