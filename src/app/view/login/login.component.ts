import { Component } from '@angular/core';
import { Usuarisclass } from "../../shared/clases/usuarisclass";
import { FormControl, FormGroup, ReactiveFormsModule, Validators } from "@angular/forms";
import { NgIf } from "@angular/common";
import { HttpClient } from "@angular/common/http";
import { ConnectDBService } from "../../shared/services/connect-db.service";
import { emailValidator, passwordValidator } from "./validar-password.directive";
import { LoginServiceService } from "../../shared/services/login-service.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  standalone: true,
  imports: [
    ReactiveFormsModule,
    NgIf
  ],
  styleUrl: './login.component.css'
})
export class LoginComponent {
  /**
   * Formulario de inicio de sesión.
   * @type FormGroup
   */
  loginForm!: FormGroup;

  /**
   * Lista de usuarios.
   * @type Usuarisclass[]
   */
  users: Usuarisclass[] = [];

  /**
   * Mensaje para mostrar al usuario.
   * @type string
   */
  message!: string;

  /**
   * Constructor del componente LoginComponent.
   * @param loginservice Servicio de login para comprobar el login del usuario.
   * @param connectbd Servicio para conectarse a la base de datos.
   */
  constructor(private connectbd: ConnectDBService, private loginservice: LoginServiceService) {}

  /**
   * Método que se ejecuta al inicializar el componente.
   * Crea el formulario de inicio de sesión.
   * @return No devuelve nada.
   */
  ngOnInit(): void {
    this.loginForm = new FormGroup({
      nom: new FormControl('',
        [Validators.minLength(7),
          Validators.required, Validators.email, emailValidator()]),
      password: new FormControl('',
        [Validators.required,
          Validators.minLength(8), passwordValidator()])
    });
  }

  /**
   * Método para iniciar sesión.
   * Envía los datos del formulario al servicio de conexión a la base de datos.
   * @return No devuelve nada.
   */
  login(): void {
    this.connectbd.Login(this.loginForm)
      .subscribe(res => {
        if (res.length == 0) {
          this.users = [];
          this.message = "Login incorrecte";
        } else {
          this.users = res;
          this.loginservice.updatLoginData(res[0].nom);
          this.message = "L'usuari " + this.users[0].nom +
            " s'ha logejat de manera correcte!!!";
        }
      });
  }

}
