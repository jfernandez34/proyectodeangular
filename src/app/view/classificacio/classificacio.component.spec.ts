import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ClassificacioComponent } from './classificacio.component';
import {RegisterComponent} from "../register/register.component";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {HttpClientTestingModule} from "@angular/common/http/testing";
import {of} from "rxjs";
import {LoginComponent} from "../login/login.component";
import {ConnectDBService} from "../../shared/services/connect-db.service";
import {HttpClient} from "@angular/common/http";

describe('ClassificacioComponent', () => {
  let component: ClassificacioComponent;
  let fixture: ComponentFixture<ClassificacioComponent>;
  let service: ConnectDBService;
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ClassificacioComponent],
      imports: [ReactiveFormsModule, HttpClientTestingModule, FormsModule],
      providers: [ConnectDBService,HttpClient]
    })
      .compileComponents();

    fixture = TestBed.createComponent(ClassificacioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
  beforeEach(() => {
    fixture = TestBed.createComponent(ClassificacioComponent);
    component = fixture.componentInstance;
    service = TestBed.inject(ConnectDBService);
    fixture.detectChanges();
  })

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('PARAMETRES MOSTRAR', () => {
    const res = {data: [{nom: 'marc@ies-sabadell.cat'}]}
    spyOn(service, 'getAllUsers').and.returnValue(of(res.data));
    component.getUsers();
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    console.log("Data: "+component.data)
    expect(component.data).toContain(jasmine.objectContaining({nom: 'marc@ies-sabadell.cat'}));
  });

});
