import { Component, ElementRef, ViewChild } from '@angular/core';
import { ConnectDBService } from "../../shared/services/connect-db.service";
import jspdf from "jspdf";
import pdfMake from "pdfmake/build/pdfmake";
import pdfFonts from "pdfmake/build/vfs_fonts";
import htmlToPdfmake from "html-to-pdfmake";
import { Usuarisclass } from "../../shared/clases/usuarisclass";

@Component({
  selector: 'app-classificacio',
  templateUrl: './classificacio.component.html',
  styleUrls: ['./classificacio.component.css']
})
export class ClassificacioComponent {

  /**
   * Usuarios obtenidos de la base de datos.
   * @type Usuarisclass[]
   */
  data!: Usuarisclass[];

  /**
   * Usuarios ordenados obtenidos de la base de datos.
   * @type Usuarisclass[]
   */
  dataordenada!: Usuarisclass[];


  /**
   * Constructor del componente ClassificacioComponent.
   * @param connectbd Servicio para conectarse a la base de datos.
   */
  constructor(private connectbd: ConnectDBService) {}

  /**
   * Referencia al elemento HTML para la generación del PDF.
   * @type ElementRef
   */
  @ViewChild('pdfTable') pdfTable!: ElementRef;

  /**
   * Método para obtener los usuarios registrados en el componente de registro.
   * @returns No devuelve nada.
   * {@link RegisterComponent}
   */
  getUsers(): void {
    console.log("entra");
    this.connectbd.getAllUsers().subscribe(res => {
      if (res.length == 0) {
        this.data = [];
        console.log("vacio");
      } else {
        this.data = res;
        console.log("else");
        //this.dataordenadafun();
      }
    });
  }

  /**
   * Método para ordenar los datos de usuarios por puntos.
   * @returns No devuelve nada.
   */
  private dataordenadafun(): void {
    this.data.sort((a, b) => b.punts - a.punts);
  }

  /**
   * Método para obtener el PDF de la clasificación.
   * @returns No devuelve nada.
   */
  obtenerPDF(): void {
    const doc = new jspdf();
    const pdfTable = this.pdfTable.nativeElement;
    pdfTable.style.display = 'table';
    var html = htmlToPdfmake(pdfTable.innerHTML);
    const documentDefinition = { content: html };
    pdfMake.createPdf(documentDefinition).open();
  }
}
