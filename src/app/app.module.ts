import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { IniciComponent } from './view/inici/inici.component';
import { JocComponent } from './view/joc/joc.component';
import { RegisterComponent } from './view/register/register.component';
import { LoginComponent } from './view/login/login.component';
import { ClassificacioComponent } from './view/classificacio/classificacio.component';
import { AjudaComponent } from './view/ajuda/ajuda.component';
import {ReactiveFormsModule} from "@angular/forms";
import {HttpClientModule} from "@angular/common/http";
import {GrupmodModule} from "./shared/modules/grupmod/grupmod.module";

@NgModule({
  declarations: [
    AppComponent,
    IniciComponent,
    JocComponent,
    RegisterComponent,
    ClassificacioComponent,
    AjudaComponent
  ],
  imports: [
    LoginComponent,
    GrupmodModule
  ],
  providers: [GrupmodModule],
  bootstrap: [AppComponent]
})
export class AppModule { }
