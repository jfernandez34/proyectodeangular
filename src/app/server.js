const express = require('express');
const app = express();
const cors = require('cors');
const bodyParser = require('body-parser');
//const {findExportedNameOfNode} = require("@angular/compiler-cli/src/ngtsc/imports/src/find_export");

app.use(cors())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({
  extended: true
}));

app.listen(3000, function () {
  console.log('Va por el puerto 3000')
})
module.exports = app;

const client = require('mongodb').MongoClient;
const url = 'mongodb://localhost:27017';
const dbName = 'albums';
const collection = 'songs';

app.get('/songs',async (req,
                        res) =>
{
  let client2;
  try {
    client2 = await client.connect(url);
    const db = client2.db(dbName);
    const songs =
      await db.collection(collection).find().toArray();
    res.json(songs);
  } catch (err) {
    console.log("Error")
  } finally {
    if (client2) {
      await client2.close()
    }
  }
}
);

app.get('/songs/rec',async (req,
                        res) =>
{
  let client2;
  let reproduccions = Number(req.query.reproduccions);
  let recaptacio = Number(req.query.recaptacio);
  try{
    client2= await client.connect(url);
    const db = client2.db(dbName);
    const songs = await db.collection(collection).
    find( {
      'Reproduccions':{$gte:reproduccions},
      'Recaptacio':{$gte:recaptacio}}).toArray();
    res.json(songs);
  }catch (err){
    console.log("error")
  } finally{
    if(client2) {
      await client2.close();
    }
  }
}
);

app.post('/songs/insert',
  async (req,
         res) => {
    let client2;
    let nom = req.body.nom;
    let reproduccions = Number(req.body.reproduccions);
    let recaptacio = Number(req.body.recaptacio);
    try {
      client2 = await client.connect(url);
      const db = client2.db(dbName);
      let id = (await db.collection('songs').find().toArray()).length + 1;

      const songs = await db.collection(collection).
      insertOne({ 'id': id, 'Nom': nom, 'Reproduccions': reproduccions, 'Recaptacio': recaptacio });
      res.json('Guardat!!!');
    } catch (err) {
      console.error("Error amb l'operació CRUD de l'API", err)
    } finally {
      if (client2) {
        client2.close();
      }
    }
  }
);

app.get('/users',async (req,
                        res) =>
  {
    let client2;
    try {
      client2 = await client.connect(url);
      const db = client2.db(dbName);
      const usuaris =
        await db.collection('usuaris_app').find().sort({punts:-1}).toArray();
      res.json(usuaris);
    } catch (err) {
      console.log("Error")
    } finally {
      if (client2) {
        await client2.close()
      }
    }
  }
);

app.get('/user/busc/busc2',async (req,
                                                   res) =>
  {
    let client2;
    let nom = req.query.nom;
    let password = req.query.password;
    try{
      client2= await client.connect(url);
      const db = client2.db(dbName);
      const usuaris = await db.collection('usuaris_app').
      find( {
        'nom':nom,
        'password':password}).toArray();
      res.json(usuaris);
    }catch (err){
      console.log("error")
    } finally{
      if(client2) {
        await client2.close();
      }
    }
  }
);

app.post('/user/insert',
  async (req,
         res) => {
    let client2;
    let nom = req.body.nom;
    let password = req.body.password;
    try {
      client2 = await client.connect(url);
      const db = client2.db(dbName);
      let id = (await db.collection('usuaris_app').find().toArray()).length + 1;

      const songs = await db.collection('usuaris_app').
      insertOne({ 'id': id, 'nom': nom, 'password': password, 'punts':0 });
      res.json('Guardat!!!');
    } catch (err) {
      console.error("Error amb l'operació CRUD de l'API", err)
    } finally {
      if (client2) {
        client2.close();
      }
    }
  }
);

app.post('/user/update',
  async (req,
         res) => {
    let client2;
    let nom = req.body.nom;
    let punts = Number(req.body.punts);
    try {
      client2 = await client.connect(url);
      const db = client2.db(dbName);
      const user = await db.collection('usuaris_app').
      updateOne({ 'nom': nom }, { $set: { 'punts': punts} });
      res.json('Actualitzat!!!');
    } catch (err) {
      console.error("Error")
    } finally {
      if (client2) {
        client2.close();
      }
    }
  }
);
