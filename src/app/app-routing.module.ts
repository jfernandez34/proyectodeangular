import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {IniciComponent} from "./view/inici/inici.component";
import {JocComponent} from "./view/joc/joc.component";
import {RegisterComponent} from "./view/register/register.component";
import {ClassificacioComponent} from "./view/classificacio/classificacio.component";
import {AjudaComponent} from "./view/ajuda/ajuda.component";
import {LoginComponent} from "./view/login/login.component";


const routes: Routes = [
  {path:'Inici', component:IniciComponent},
  {path:'Joc', component:JocComponent},
  {path:'Login', component:LoginComponent},
  {path:'Register', component:RegisterComponent},
  {path:'Classificació', component:ClassificacioComponent},
  {path:'Ajuda', component:AjudaComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
