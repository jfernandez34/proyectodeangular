import { Component } from '@angular/core';
import {LoginServiceService} from "./shared/services/login-service.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrl: './app.component.css'
})
export class AppComponent {
  private usuari: any;

  constructor( private loginservice: LoginServiceService) {
    this.loginservice.userlogin.subscribe((data:string)=>this.usuari = data)
  }
  ngOnInit(){
    this.usuari = this.loginservice.userlogin.getValue();
    console.log(this.usuari);
  }
  title = 'ProyectoDeAngular';
}
