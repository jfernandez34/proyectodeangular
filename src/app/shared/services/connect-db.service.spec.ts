import {ComponentFixture, TestBed} from '@angular/core/testing';

import { ConnectDBService } from './connect-db.service';
import {HttpClient} from "@angular/common/http";
import {HttpClientTestingModule, HttpTestingController} from "@angular/common/http/testing";
import {FormBuilder, FormControl, FormGroup, FormsModule, ReactiveFormsModule} from "@angular/forms";
import {of} from "rxjs";
import {ClassificacioComponent} from "../../view/classificacio/classificacio.component";
import {LoginComponent} from "../../view/login/login.component";
import {JocComponent} from "../../view/joc/joc.component";

describe('ConnectDBService', () => {
  let service: ConnectDBService;
  let formBuilder: FormBuilder;
  let component:ClassificacioComponent;
  let fixture: ComponentFixture<ClassificacioComponent>;
  let component2:LoginComponent;
  let fixture2: ComponentFixture<LoginComponent>;
  let component3:JocComponent;
  let fixture3: ComponentFixture<JocComponent>;
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ClassificacioComponent, JocComponent],
      imports: [HttpClientTestingModule, FormsModule, ReactiveFormsModule, LoginComponent],
      providers: [ConnectDBService, HttpClient]
    }).compileComponents();

    fixture = TestBed.createComponent(ClassificacioComponent);
    fixture2 = TestBed.createComponent(LoginComponent);
    fixture3 = TestBed.createComponent(JocComponent);
    component = fixture.componentInstance;
    component2 = fixture2.componentInstance;
    component3 = fixture3.componentInstance;
    service = TestBed.inject(ConnectDBService);
    fixture.detectChanges();
    fixture2.detectChanges();
    fixture3.detectChanges();
  });
  beforeEach(() =>{
    fixture = TestBed.createComponent(ClassificacioComponent);
    fixture2 = TestBed.createComponent(LoginComponent);
    fixture3 = TestBed.createComponent(JocComponent);
    component = fixture.componentInstance;
    component2 = fixture2.componentInstance;
    component3 = fixture3.componentInstance;
    service = TestBed.inject(ConnectDBService);
    formBuilder = TestBed.inject(FormBuilder);
    fixture.detectChanges();
    fixture2.detectChanges();
    fixture3.detectChanges();
  })
  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  /**
   *
   * MOSTRAR
   *
   */

  it('URL Y METODO MOSTRAR', () => {
    service.getAllUsers().subscribe(users => {
    });
    let httpTestingController = TestBed.inject(HttpTestingController);
    const req = httpTestingController.expectOne(service.REST_API + '/users');
    expect(req.request.method).toEqual('GET');
  });

  /**
   *
   * LOGIN
   *
   */

  it('URL Y METODO LOGIN', () => {
    let myForm = formBuilder.group({
      nom: new FormControl('pruebanom'),
      password: new FormControl('pruebapass')
    })
    service.Login(myForm).subscribe(users => {
    });
    let httpTestingController = TestBed.inject(HttpTestingController);
    const req = httpTestingController.expectOne(service.REST_API + '/user/busc/busc2?nom=pruebanom&password=pruebapass');
    expect(req.request.method).toEqual('GET');
  });

  /**
   *
   * ACTUALITZAR
   *
   */

  it('URL Y METODO ACTUALIZAR', () => {
    let myForm = formBuilder.group({
      nom: new FormControl('pruebanom'),
      punts: new FormControl(10)
    })
    service.updatePunts(myForm).subscribe(users => {
    });
    let httpTestingController = TestBed.inject(HttpTestingController);

    const req = httpTestingController.expectOne(service.REST_API + '/user/update');
    expect(req.request.method).toEqual('POST');
  });

/**
*
* REGISTER
*
*/

  it('URL Y METODO REGISTER', () => {
    let myForm = formBuilder.group({
      nom: new FormControl('pruebanom'),
      punts: new FormControl('pruebapasswd')
    })
    service.setUser(myForm).subscribe(users => {
    });
    let httpTestingController = TestBed.inject(HttpTestingController);
    const req = httpTestingController.expectOne(service.REST_API + '/user/insert');
    expect(req.request.method).toEqual('POST');
    });
});
