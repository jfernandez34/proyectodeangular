import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {FormGroup} from "@angular/forms";

@Injectable({
  providedIn: 'root'
})
export class ConnectDBService {
  REST_API: string = 'http://localhost:3000';
  constructor(private httpclient:HttpClient) { }
  public Login(form:FormGroup):Observable<any>{
    return this.httpclient.get(`${this.REST_API}/user/busc/busc2`,
      {params:{nom:form.controls['nom'].value,
        password: form.controls['password'].value}})
  }
  public setUser(form : FormGroup){
    console.log(form.value)
    return this.httpclient.post(`${this.REST_API}/user/insert`, form.value)
  }
  public getAllUsers():Observable<any>{
    return this.httpclient.get(`${this.REST_API}/users`);
  }
  public updatePunts(form:FormGroup){
    return this.httpclient.post(`${this.REST_API}/user/update`, form.value)
  }
}
