import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {BrowserModule} from "@angular/platform-browser";
import {AppRoutingModule} from "../../../app-routing.module";
import {ReactiveFormsModule} from "@angular/forms";
import {LoginComponent} from "../../../view/login/login.component";
import {HttpClientModule} from "@angular/common/http";
import {ConnectDBService} from "../../services/connect-db.service";
import { ValidarPasswordDirective } from '../../../view/login/validar-password.directive';



@NgModule({
  declarations: [
    ValidarPasswordDirective
  ],
  imports: [
    CommonModule
  ],
  exports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    //IMPORTANTE
    HttpClientModule,
    ReactiveFormsModule,
    ValidarPasswordDirective,
    //IMPORTANTE
  ],
  providers: [ConnectDBService]
})
export class GrupmodModule { }
